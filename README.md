The Unfettered Keyboard is an on-screen keyboard for Linux mobile.

# User Interfaces (UIs)

## Phosh

Phosh has a D-Bus API for showing the keyboard without focusing on a text box.
The keyboard can be shown by making a long press on the bottom of the screen.
The API tells the keyboard to activate while making text-emitting keys fall
back to emitting key presses.

Phoc, the compositor, does not implement all Wayland text input protocols. Most
Qt applications require manually activating the keyboard and can only accept
keys supported by xkbcommon (e.g. no "‹", "›", "₹", "₽", "₴", "₿", or "₱").

### Build target

The `ufkbd-gnome` build target is for Phosh.

### Switching keyboards

The keyboard program is specified in the `Exec=` line of
`/usr/share/applications/sm.puri.OSK0.desktop`. Change it to the absolute path
of your keyboard.

## Plasma Mobile

Plasma Mobile has no mechanism to show or hide the keyboard without a text
field.

Kwin does not implement the Wayland virtual keyboard protocol. Most
applications can only accept text and do not handle the key events that an
input method can emit (i.e. no Ctrl or Alt modifiers, no Enter key, no Escape
key, Tab only works with Fn, no arrow keys).

### Build target

The `ufkbd-wl` build target is for Plasma Mobile.

### Switching keyboards

In your $PATH (usually includes `/usr/local/bin`), Plasma Mobile looks for
`maliit-keyboard`. You can symlink it to your desired keyboard.

Alternatively, you could edit the `startplasmamobile` script and replace
`maliit-keyboard` with your desired on-screen keyboard, but this may require
adding support for your keyboard to KDE.

## SXMO (Wayland)

SXMO shows and hides the on-screen keyboard by running and terminating it. The
on-screen keyboard does not handle visibility.

### Build target

The `ufkbd-sxmo` build target is for SXMO.

### Switching keyboards

Set the $KEYBOARD environment variable to your desired keyboard.

## Other

Most Wayland compositors have no mechanism to show or hide the keyboard without
a text field.

### Build target

The `ufkbd-wl` build target is for most compatible Wayland compositors.

### Switching keyboards

Configure your desired keyboard to run on startup. This may mean a desktop file
in `~/.config/autostart`.

# Installation

This program has the following build dependencies:

- [libexpat](https://libexpat.github.io) (Alpine package: `expat-dev`),
  the fast streaming XML parser written in C99 with >90% test coverage.
- [libclang](https://clang.llvm.org) (Alpine package: `clang-libclang`),
  a compiler front-end for the C family of languages (C, C++, Objective-C, and
  Objective-C++).

You can install it with:

    $ cargo install --bin <target>

Layouts are essential and not installed by cargo. They are installed manually:

    $ sudo mkdir -p /usr/share/unfettered-keyboard/layouts
    $ curl -OL https://github.com/Julow/Unexpected-Keyboard/raw/1.29.1/srcs/layouts/latn_qwerty_us.xml
    $ sudo mv latn_qwerty_us.xml /usr/share/unfettered-keyboard/layouts
    $ curl -OL https://github.com/Julow/Unexpected-Keyboard/raw/1.29.1/res/xml/bottom_row.xml
    $ sudo mv bottom_row.xml /usr/share/unfettered-keyboard/layouts
    $ curl -OL https://github.com/Julow/Unexpected-Keyboard/raw/1.29.1/res/xml/numeric.xml
    $ sudo mv numeric.xml /usr/share/unfettered-keyboard/layouts
    $ curl -OL https://github.com/Julow/Unexpected-Keyboard/raw/1.29.1/res/xml/greekmath.xml
    $ sudo mv greekmath.xml /usr/share/unfettered-keyboard/layouts

# Configuration

This keyboard is configured with [YAML](https://yaml.org).

Configuration example:

    %YAML 1.2
    ---
    layout: latn_qwerty_us.xml
    
    longpress_ms: 600
    repeat_ms: 25
    
    wayland:
      height: 185
      input-method: true

# License

Unfettered Keyboard is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

Prior to version 2.0.0, Gitlab automatically displayed "3.0 or later", but that
was not the intended license as seen in the source code files. Version 2.0.0
ensures that the wrong license would not be displayed on Gitlab.
