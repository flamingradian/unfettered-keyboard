// SPDX-License-Identifier: GPL-3.0-only
/*
 * Copyright (c) 2024, Richard Acayan. All rights reserved.
 */

use crate::core::Keyboard;
use crate::core::Layout;
use crate::core::Part;
use crate::wayland::input_method_unstable_v1::zwp_input_method_context_v1::ZwpInputMethodContextV1;
use crate::wayland::input_method_unstable_v2::zwp_input_method_manager_v2::ZwpInputMethodManagerV2;
use crate::wayland::input_method_unstable_v2::zwp_input_method_v2::ZwpInputMethodV2;
use crate::wayland::virtual_keyboard_unstable_v1::zwp_virtual_keyboard_manager_v1::ZwpVirtualKeyboardManagerV1;
use crate::wayland::virtual_keyboard_unstable_v1::zwp_virtual_keyboard_v1::ZwpVirtualKeyboardV1;
use reis::ei;
use std::collections::HashMap;
use std::fs;
use std::io::Seek;
use std::io::Write;
use std::os::fd::AsFd;
use std::process;
use wayland_client::Dispatch;
use wayland_client::QueueHandle;
use wayland_client::protocol::wl_seat::WlSeat;
use wayland_client::protocol::wl_keyboard::KeymapFormat;
use xkeysym::Keysym;

const STATIC_KEYCODES: [(Keysym, u8); 76] = [
    (Keysym::space, 65),
    (Keysym::apostrophe, 48),
    (Keysym::comma, 59),
    (Keysym::minus, 20),
    (Keysym::period, 60),
    (Keysym::slash, 61),
    (Keysym::_0, 19),
    (Keysym::_1, 10),
    (Keysym::_2, 11),
    (Keysym::_3, 12),
    (Keysym::_4, 13),
    (Keysym::_5, 14),
    (Keysym::_6, 15),
    (Keysym::_7, 16),
    (Keysym::_8, 17),
    (Keysym::_9, 18),
    (Keysym::semicolon, 47),
    (Keysym::equal, 21),
    (Keysym::bracketleft, 34),
    (Keysym::backslash, 51),
    (Keysym::bracketright, 35),
    (Keysym::grave, 49),
    (Keysym::a, 38),
    (Keysym::b, 56),
    (Keysym::c, 54),
    (Keysym::d, 40),
    (Keysym::e, 26),
    (Keysym::f, 41),
    (Keysym::g, 42),
    (Keysym::h, 43),
    (Keysym::i, 31),
    (Keysym::j, 44),
    (Keysym::k, 45),
    (Keysym::l, 46),
    (Keysym::m, 58),
    (Keysym::n, 57),
    (Keysym::o, 32),
    (Keysym::p, 33),
    (Keysym::q, 24),
    (Keysym::r, 27),
    (Keysym::s, 39),
    (Keysym::t, 28),
    (Keysym::u, 30),
    (Keysym::v, 55),
    (Keysym::w, 25),
    (Keysym::x, 53),
    (Keysym::y, 29),
    (Keysym::z, 52),
    (Keysym::BackSpace, 22),
    (Keysym::Tab, 23),
    (Keysym::Return, 36),
    (Keysym::Escape, 9),
    (Keysym::Home, 110),
    (Keysym::Left, 113),
    (Keysym::Up, 111),
    (Keysym::Right, 114),
    (Keysym::Down, 116),
    (Keysym::Prior, 112),
    (Keysym::Next, 117),
    (Keysym::End, 115),
    (Keysym::Insert, 118),
    (Keysym::F1, 67),
    (Keysym::F2, 68),
    (Keysym::F3, 69),
    (Keysym::F4, 70),
    (Keysym::F5, 71),
    (Keysym::F6, 72),
    (Keysym::F7, 73),
    (Keysym::F8, 74),
    (Keysym::F9, 75),
    (Keysym::F10, 76),
    (Keysym::F11, 95),
    (Keysym::F12, 96),
    (Keysym::Shift_L, 50),
    (Keysym::Control_L, 37),
    (Keysym::Delete, 119),
];

pub struct VirtualKeyboard {
    vk: Option<ZwpVirtualKeyboardV1>,
    keymap_id: u8,

    im: Option<ZwpInputMethodV2>,
    im_ctx: Option<ZwpInputMethodContextV1>,
    ei_kbd: Option<(ei::device::Device, ei::keyboard::Keyboard)>,
    im_serial: u32,
    ei_serial: u32,

    keycodes: HashMap<Keysym, u8>,
    pressed: [Keysym; 248],
    mod_state: u32,
}

impl VirtualKeyboard {
    pub fn new<T: Dispatch<ZwpVirtualKeyboardV1, ()>
                + Dispatch<ZwpInputMethodV2, ()>
                + 'static>(queue: &QueueHandle<T>,
                               vk_man: &Option<ZwpVirtualKeyboardManagerV1>,
                               im_man: &Option<ZwpInputMethodManagerV2>,
                               seat: &WlSeat)
                            -> VirtualKeyboard
    {
        let vk = vk_man.as_ref().map(|m| m.create_virtual_keyboard(seat, queue, ()));
        let im = im_man.as_ref().map(|m| m.get_input_method(seat, queue, ()));

        VirtualKeyboard {
            vk,
            keymap_id: 1,

            im,
            im_ctx: None,
            ei_kbd: None,
            im_serial: 0,
            ei_serial: 0,

            keycodes: HashMap::with_capacity(248),
            pressed: [Keysym::NoSymbol; 248],
            mod_state: 0,
        }
    }

    fn write_key(keymap: &mut fs::File, part: &Part, keycode: u8)
    {
        let sym = part.sym();
        let lower = &sym.name().unwrap()[3..];
        let upper = &Part::modify_shift(sym).name().unwrap()[3..];

        writeln!(keymap, "    key <I{:03}> {{ [ {}, {} ] }};",
                 keycode, lower, upper).unwrap();

        if sym == Keysym::Shift_L {
            writeln!(keymap,
                     "    modifier_map Shift {{ <I{:03}> }};",
                     keycode).unwrap();
        } else if sym == Keysym::Control_L {
            writeln!(keymap,
                     "    modifier_map Control {{ <I{:03}> }};",
                     keycode).unwrap();
        } else if sym == Keysym::Alt_L {
            writeln!(keymap,
                     "    modifier_map Mod1 {{ <I{:03}> }};",
                     keycode).unwrap();
        }
    }

    pub fn done(&mut self)
    {
        self.im_serial += 1;
    }

    pub fn set_im_serial(&mut self, serial: u32)
    {
        self.im_serial = serial;
    }

    pub fn set_ei_serial(&mut self, serial: u32)
    {
        self.ei_serial = serial;
    }

    pub fn set_input_method_context(&mut self, imc: Option<ZwpInputMethodContextV1>)
    {
        self.im_ctx = imc;
    }

    pub fn set_ei_keyboard(&mut self,
                           dev: ei::device::Device,
                           kbd: ei::keyboard::Keyboard)
    {
        self.ei_kbd = Some((dev, kbd));
    }
}

impl Keyboard for VirtualKeyboard {
    fn key_supported(&self, sym: Keysym) -> bool
    {
        if self.vk.is_some() {
            match sym.name() {
                Some(n) => n.starts_with("XK_"),
                None => false,
            }
        } else if self.ei_kbd.is_some() {
            STATIC_KEYCODES.binary_search_by_key(&sym, |(s, _)| *s).is_ok()
        } else {
            self.im_ctx.is_some()
        }
    }

    fn press(&mut self, sym: Keysym)
    {
        match sym {
            Keysym::Shift_L => {
                self.mod_state |= 0x1;
            },
            Keysym::Control_L => {
                self.mod_state |= 0x4;
            },
            Keysym::Alt_L => {
                self.mod_state |= 0x8;
            },
            _ => (),
        }

        if let Some(vk) = &self.vk {
            if sym == Keysym::Shift_L
            || sym == Keysym::Control_L
            || sym == Keysym::Alt_L {
                vk.modifiers(self.mod_state, 0, 0, 0);
            }

            let keycode = *self.keycodes.get(&sym).unwrap();
            self.pressed[keycode as usize] = sym;
            vk.key(0, keycode as u32, 1);
        } else if let Some((d, k)) = &self.ei_kbd {
            let i = STATIC_KEYCODES.binary_search_by_key(&sym, |(s, _)| *s).unwrap();
            let keycode = STATIC_KEYCODES[i].1 - 8;
            k.key(keycode as u32, ei::keyboard::KeyState::Press);
            d.frame(self.ei_serial, 0);
        } else if let Some(imc) = &self.im_ctx {
            imc.keysym(self.im_serial, 0, sym.raw(), 0, self.mod_state);
        }
    }

    fn release(&mut self, sym: Keysym)
    {
        match sym {
            Keysym::Shift_L => {
                self.mod_state &= !0x1;
            },
            Keysym::Control_L => {
                self.mod_state &= !0x4;
            },
            Keysym::Alt_L => {
                self.mod_state &= !0x8;
            },
            _ => (),
        }

        if let Some(vk) = &self.vk {
            if sym == Keysym::Shift_L
            || sym == Keysym::Control_L
            || sym == Keysym::Alt_L {
                vk.modifiers(self.mod_state, 0, 0, 0);
            }

            let keycode = *self.keycodes.get(&sym).unwrap();
            self.pressed[keycode as usize] = Keysym::NoSymbol;
            vk.key(0, keycode as u32, 0);
        } else if let Some((d, k)) = &self.ei_kbd {
            let i = STATIC_KEYCODES.binary_search_by_key(&sym, |(s, _)| *s).unwrap();
            let keycode = STATIC_KEYCODES[i].1 - 8;
            k.key(keycode as u32, ei::keyboard::KeyState::Released);
            d.frame(self.ei_serial, 0);
        } else if let Some(imc) = &self.im_ctx {
            imc.keysym(self.im_serial, 0, sym.raw(), 0, self.mod_state);
        }
    }

    fn text(&mut self, text: &str)
    {
        if let Some(im) = &self.im {
            im.commit_string(text.to_string());
            im.commit(self.im_serial);
        } else if let Some(imc) = &self.im_ctx {
            imc.commit_string(self.im_serial, text.to_string());
        } else {
            panic!("Text should not be emitted without an input method protocol");
        }
    }

    fn change_layout(&mut self, layout: &Layout)
    {
        let mut keycode = 8;
        let mut used_codes = [false; 248];

        self.keymap_id = match self.keymap_id {
            0 => 1,
            1 => 0,
            _ => panic!("There should only be up to two keymaps"),
        };

        let path = format!("/tmp/ufkbd-keymap-pid{}-{}",
                           process::id(), self.keymap_id);
        let mut keymap = fs::File::create_new(path.clone()).unwrap();
        let _ = fs::remove_file(path);

        keymap.write_all(b"xkb_keymap {\n").unwrap();
        keymap.write_all(b"  xkb_symbols \"ufkbd\" {\n").unwrap();

        self.keycodes.clear();
        for row in layout.rows() {
            for key in row {
                for part in &key.parts {
                    if !part.key_available() {
                        continue;
                    }

                    if let Ok(idx) = STATIC_KEYCODES.binary_search_by_key(&part.sym(), |(s, _)| *s) {
                        let (sym, code) = STATIC_KEYCODES[idx];
                        self.keycodes.insert(sym, code - 8);
                        Self::write_key(&mut keymap, part, code);
                        used_codes[code as usize - 8] = true;
                    }
                }
            }
        }

        for row in layout.rows() {
            for key in row {
                for part in &key.parts {
                    if !part.key_available() {
                        continue;
                    }

                    if self.keycodes.contains_key(&part.sym()) {
                        continue;
                    }

                    while used_codes[keycode as usize - 8] {
                        keycode += 1;
                    }

                    self.keycodes.insert(part.sym(), keycode - 8);
                    Self::write_key(&mut keymap, part, keycode);
                    keycode += 1;
                }
            }
        }

        keymap.write_all(b"  };\n").unwrap();
        keymap.write_all(b"\n").unwrap();
        keymap.write_all(b"  xkb_keycodes \"ufkbd\" {\n").unwrap();
        keymap.write_all(b"    minimum = 8;\n").unwrap();
        keymap.write_all(b"    maximum = 255;\n").unwrap();

        for i in 8..keycode {
            writeln!(keymap, "    <I{:03}> = {};", i, i).unwrap();
        }

        for i in keycode..255 {
            if used_codes[i as usize - 8] {
                writeln!(keymap, "    <I{:03}> = {};", i, i).unwrap();
            }
        }

        keymap.write_all(b"    indicator 1 = \"Caps Lock\";\n").unwrap();
        keymap.write_all(b"  };\n").unwrap();
        keymap.write_all(b"\n").unwrap();
        keymap.write_all(b"  xkb_types \"ufkbd\" {\n").unwrap();
        keymap.write_all(b"    type \"TWO_LEVEL\" {\n").unwrap();
        keymap.write_all(b"        modifiers = Shift;\n").unwrap();
        keymap.write_all(b"        map[Shift] = Level2;\n").unwrap();
        keymap.write_all(b"        level_name[Level1] = \"Base\";\n").unwrap();
        keymap.write_all(b"        level_name[Level2] = \"Shift\";\n").unwrap();
        keymap.write_all(b"    };\n").unwrap();
        keymap.write_all(b"  };\n").unwrap();
        keymap.write_all(b"\n").unwrap();
        keymap.write_all(b"  xkb_compatibility \"ufkbd\" {\n").unwrap();
        keymap.write_all(b"  };\n").unwrap();
        keymap.write_all(b"};\n").unwrap();

        if let Some(vk) = self.vk.as_ref() {
            if self.mod_state != 0 {
                vk.modifiers(0, 0, 0, 0);
            }

            for (code, sym) in self.pressed.iter().enumerate() {
                if *sym != Keysym::NoSymbol {
                    vk.key(0, code as u32, 0);
                }
            }

            let len = keymap.stream_position().unwrap() as u32;
            vk.keymap(KeymapFormat::XkbV1 as u32, keymap.as_fd(), len);

            if self.mod_state != 0 {
                vk.modifiers(self.mod_state, 0, 0, 0);
            }

            for sym in &self.pressed {
                if *sym != Keysym::NoSymbol {
                    let code = *self.keycodes.get(sym).unwrap();
                    vk.key(0, code as u32, 1);
                }
            }
        }
    }
}
