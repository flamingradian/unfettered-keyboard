// SPDX-License-Identifier: GPL-3.0-only
/*
 * Copyright (c) 2024, Richard Acayan. All rights reserved.
 */

mod core;
mod wayland;

use crate::core::Configuration;
use crate::wayland::Dispatcher;
use polling::Event;
use polling::Events;
use polling::Poller;
use std::mem;
use std::time::Instant;
use wayland_client::globals;

pub struct VisibilityManager();

impl VisibilityManager {
    pub fn set_visible(&self, _visible: bool)
    {
        // SXMO manages visibility
    }
}

fn main()
{
    let conn = wayland_client::Connection::connect_to_env().unwrap();

    let (globals, mut queue) = globals::registry_queue_init::<Dispatcher>(&conn)
                                            .expect("Registry required");

    let config = Configuration::load().unwrap();

    let mut dispatcher = Dispatcher::new(&config, queue.handle(), &globals).unwrap();
    dispatcher.set_osk(VisibilityManager());

    let gfx = dispatcher.graphics();
    let mut gfx = gfx.lock().unwrap();
    gfx.display_mut().show();
    mem::drop(gfx);

    let wl_evt = Event::readable(0);

    let mut events = Events::new();
    let poller = Poller::new().unwrap();

    loop {
        conn.flush().unwrap();

        let guard = queue.prepare_read().unwrap();
        let fd = guard.connection_fd();
        let timer = dispatcher.button().next_time().map(|t| t - Instant::now());

        unsafe {
            poller.add(&fd, wl_evt).unwrap();
        }

        events.clear();
        poller.wait(&mut events, timer).unwrap();
        poller.delete(fd).unwrap();

        if !events.is_empty() {
            // This may still emit EWOULDBLOCK errors.
            let _ = guard.read();
            queue.dispatch_pending(&mut dispatcher).unwrap();
        }

        dispatcher.dispatch_timers();
    }
}
