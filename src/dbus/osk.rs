// SPDX-License-Identifier: GPL-3.0-only
/*
 * Copyright (c) 2024, Richard Acayan. All rights reserved.
 */

use crate::core::Graphics;
use crate::wayland::Surface;
use crate::wayland::fractional_scale_v1::wp_fractional_scale_v1::WpFractionalScaleV1;
use crate::wayland::viewporter::wp_viewport::WpViewport;
use crate::wayland::wlr_layer_shell_unstable_v1::zwlr_layer_surface_v1::ZwlrLayerSurfaceV1;
use std::sync::Arc;
use std::sync::Mutex;
use std::sync::atomic::AtomicBool;
use std::sync::atomic::Ordering;
use wayland_client::Dispatch;
use wayland_client::protocol::wl_buffer::WlBuffer;
use wayland_client::protocol::wl_shm_pool::WlShmPool;
use wayland_client::protocol::wl_surface::WlSurface;
use zbus::Connection;
use zbus::Result;
use zbus::interface;
use zbus::object_server::InterfaceRef;
use zbus::object_server::SignalEmitter;

pub struct OSK0<T: Dispatch<WlBuffer, u32>
                 + Dispatch<WlShmPool, u32>
                 + Dispatch<WlSurface, ()>
                 + Dispatch<ZwlrLayerSurfaceV1, ()>
                 + Dispatch<WpFractionalScaleV1, ()>
                 + Dispatch<WpViewport, ()>
                 + 'static> {
    visible: AtomicBool,
    gfx: Arc<Mutex<Graphics<Surface<T>>>>,
    sigctx: SignalEmitter<'static>,
    conn_wl: Arc<wayland_client::Connection>,
}

#[interface(name = "sm.puri.OSK0")]
impl<T: Dispatch<WlBuffer, u32>
      + Dispatch<WlShmPool, u32>
      + Dispatch<WlSurface, ()>
      + Dispatch<ZwlrLayerSurfaceV1, ()>
      + Dispatch<WpFractionalScaleV1, ()>
      + Dispatch<WpViewport, ()>
      + 'static> OSK0<T> {
    fn get_visible(&self) -> bool
    {
        self.visible.load(Ordering::Relaxed)
    }

    pub async fn set_visible(&self, visible: bool)
    {
        self.visible.store(visible, Ordering::Relaxed);
        self.visible_changed(&self.sigctx).await.unwrap();

        let mut gfx = self.gfx.lock().unwrap();
        let disp = gfx.display_mut();
        if visible {
            disp.show();
        } else {
            disp.hide();
        }

        self.conn_wl.flush().unwrap();
    }

    #[zbus(property)]
    fn visible(&self) -> bool
    {
        self.visible.load(Ordering::Relaxed)
    }
}

impl<T: Dispatch<WlBuffer, u32>
      + Dispatch<WlShmPool, u32>
      + Dispatch<WlSurface, ()>
      + Dispatch<ZwlrLayerSurfaceV1, ()>
      + Dispatch<WpFractionalScaleV1, ()>
      + Dispatch<WpViewport, ()>
      + 'static> OSK0<T> {
    pub async fn start(conn: &Connection,
                       gfx: Arc<Mutex<Graphics<Surface<T>>>>,
                       conn_wl: Arc<wayland_client::Connection>)
                    -> Result<InterfaceRef<OSK0<T>>>
    {
        let sigctx = SignalEmitter::new(conn, "/sm/puri/OSK0")?;

        let visible = AtomicBool::new(false);
        let osk = OSK0 { visible, gfx, sigctx, conn_wl };

        let server = conn.object_server();
        server.at("/sm/puri/OSK0", osk).await?;
        conn.request_name("sm.puri.OSK0").await?;

        let osk = server.interface("/sm/puri/OSK0").await?;

        Ok(osk)
    }
}
