// SPDX-License-Identifier: GPL-3.0-only
/*
 * Copyright (c) 2024, Richard Acayan. All rights reserved.
 */

mod core;
mod wayland;

use ashpd::desktop::PersistMode;
use ashpd::desktop::remote_desktop::DeviceType;
use ashpd::desktop::remote_desktop::RemoteDesktop;
use crate::core::Configuration;
use crate::wayland::Dispatcher;
use polling::Event;
use polling::Events;
use polling::Poller;
use std::time::Instant;
use reis::ei::Context;
use reis::ei::handshake::ContextType;
use reis::handshake;
use tokio::task;
use wayland_client::globals;

pub struct VisibilityManager();

impl VisibilityManager {
    pub fn set_visible(&self, _visible: bool)
    {
        unreachable!();
    }
}

#[tokio::main(flavor = "current_thread")]
async fn main()
{
    let conn = wayland_client::Connection::connect_to_env().unwrap();

    let (globals, mut queue) = globals::registry_queue_init::<Dispatcher>(&conn)
                                            .expect("Registry required");

    let config = Configuration::load().unwrap();

    let mut dispatcher = Dispatcher::new(&config, queue.handle(), &globals).unwrap();

    let wl_evt = Event::readable(0);

    let mut events = Events::new();
    let poller = Poller::new().unwrap();

    let rd = RemoteDesktop::new().await.unwrap();
    let session = rd.create_session().await.unwrap();
    rd.select_devices(&session,
                      DeviceType::Keyboard.into(),
                      None,
                      PersistMode::DoNot).await.unwrap();
    rd.start(&session, None).await.unwrap();
    let eifd = rd.connect_to_eis(&session).await.unwrap();
    let ei_ctx = Context::new(eifd.into()).unwrap();

    let ei_evt = Event::readable(1);

    task::spawn_blocking(move || {
        handshake::ei_handshake_blocking(&ei_ctx, "ufkbd-kde",
                                         ContextType::Sender).unwrap();

        loop {
            conn.flush().unwrap();
            ei_ctx.flush().unwrap();

            let guard = queue.prepare_read().unwrap();
            let wlfd = guard.connection_fd();
            let timer = dispatcher.button().next_time().map(|t| t - Instant::now());

            unsafe {
                poller.add(&wlfd, wl_evt).unwrap();
            }

            unsafe {
                poller.add(&ei_ctx, ei_evt).unwrap();
            }

            events.clear();
            poller.wait(&mut events, timer).unwrap();
            poller.delete(wlfd).unwrap();
            poller.delete(&ei_ctx).unwrap();

            if !events.is_empty() {
                let _ = guard.read();
                let _ = ei_ctx.read();

                queue.dispatch_pending(&mut dispatcher).unwrap();
                dispatcher.dispatch_ei(&ei_ctx);
            }

            dispatcher.dispatch_timers();
        }
    }).await.unwrap();
}
